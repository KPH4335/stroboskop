# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://KPH4335@bitbucket.org/KPH4335/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/KPH4335/stroboskop/commits/36cfe0ca2d909ecd92e5b68387f4f441255a6f13

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/KPH4335/stroboskop/commits/e5c656aeb7ccd8b97414f203133976632acf3211

Naloga 6.3.2:
https://bitbucket.org/KPH4335/stroboskop/commits/47d6e91eb5bb6b09e6c1dd04686202a17d11e0ab

Naloga 6.3.3:
https://bitbucket.org/KPH4335/stroboskop/commits/23fb8575fb1ba11ed07ebc6b51e36ba263af03ff

Naloga 6.3.4:
https://bitbucket.org/KPH4335/stroboskop/commits/a9f34bc83b51a5f50852307eb1c817de52a72be8

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/KPH4335/stroboskop/commits/1f61f365db580bcf21997a409449ea565d3b80a6

Naloga 6.4.2:
https://bitbucket.org/KPH4335/stroboskop/commits/65110eadc4add13e45d1936cb8488104c44c600d

Naloga 6.4.3:
https://bitbucket.org/KPH4335/stroboskop/commits/235f9da671ecba85cb48696ea55a3c82f637f77d

Naloga 6.4.4:
https://bitbucket.org/KPH4335/stroboskop/commits/c35bbb22229453fd9bf194f3a705822d79c7773a